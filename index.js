const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore()

//verify an employee
app.get('/employees/:email/verify', async (req, res)=>{
    const key = datastore.key(['employees',req.params.email]);
    const employee = await datastore.get(key);
    if(employee[0] === undefined )
    {
        res.send({error: "User does not exists"});
    }
    else 
    {
        const response = {fname: employee[0].fname, lname: employee[0].lname, password: employee[0].password};
        console.log(response);
        res.send(response);
    }
});

//get all employees
app.get('/employees', async (req, res)=>{
    const key = datastore.createQuery('employees');
    const [employees] = await datastore.runQuery(key); 
    res.send(employees);
    
});

const PORT = process.env.PORT || 3002;
app.listen(PORT,()=>console.log('Application Started'));




//NOTES 
//minimum in app.yaml is runtime and service and default

// steps: 
// 1) DataStore - create entities
// 2) Create app engine instance - configure app.yaml
// 3) gcloud deploy should creaate an instance for you - 
// 4) must have a default instance
// 5) 1st time - deploying app.yaml using no service parameter

// **CORS** - requesting to API from a different IP instead of current, to establish connection between the two
//
